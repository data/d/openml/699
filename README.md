# OpenML dataset: disclosure_z

https://www.openml.org/d/699

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Data Used in "A BAYESIAN APPROACH TO DATA DISCLOSURE: OPTIMAL
INTRUDER BEHAVIOR FOR CONTINUOUS DATA"
by Stephen E. Fienberg, Udi E. Makov, and Ashish P. Sanil

Background:
==========
In this paper we develop an approach to data disclosure in survey settings by
adopting a probabilistic definition of disclosure due to Dalenius. Our approach
is based on the principle that a data collection agency must consider
disclosure from the perspective of an intruder in order to efficiently evaluate
data disclosure limitation procedures. The probabilistic definition and our
attempt to study optimal intruder behavior lead naturally to a Bayesian
formulation.  We apply the methods in a small-scale simulation study using data
adapted from an actual survey conducted by the Institute for Social Research at
York University. (See Sections 1-3 of the paper for details oF the model
formulation and related issues.)

The Data:
========
Our case study uses data from the survey data Elite Canadian
Decision-Makers collected by the Institute for Social Research at York
University.  This survey was conducted in 1981 using telephone
interviews and there were 1348 respondents, but many of these did not
supply complete data.  We have extracted data on 12 variables, each of which
was measured on a 5-point scale:

Civil-liberties:
- ---------------
C1 - Free speech is just not worth it.
C2 - We have gone too far in pushing equal rights in this country.
C3 - It is better to live in an  orderly  society  than  to  allow people so
much freedom.
C5 - Free speech ought to be allowed for all political groups.

Attitudes towards Jews:
- ----------------------
A15 - Most Jews don't care what happens to people who are not Jews.
A18 - Jews are more willing than others to use shady practices  to
get ahead.

Canada-US relationship:
- ----------------------
CUS1 - Ensure independent Canada.
CUS5 - Canada should have free trade with the USA.
CUS6 - Canada's way of life is influenced strongly by USA.
CUS7 - Canada benefits from US investments.


In addition, we have data on two approximately continuous variables:

Personal information:
- --------------------
Income - Total family income before taxes (with top-coding at \$80,000).
Age - Based on year of birth.


We transformed the original survey data  as follows in order to create
a database of approximately continuous variables:

[A]  We add categorical  variables  (all  but  income) to increase the number
of levels. (When necessary we reversed the order of levels of a response  to a
question.)  The new variables are defined as follows:

Civil     = C1 + C2 + C3 + (8 - C5)
Attitude  = A15 + A18
Can/US    = (5 - CUS1) + CUS5 + (5 - CUS6) + CUS7

After we removed cases with missing observations and  two  cases involving
young children, we had a  data-base consisting of 662 observations.

[B]  In order to enhance continuity, we took the following measures:

Age:  We added normal  distributed  variates,  with  0   mean  and
variance 4 to all observations.
Income: We added uniform variates on the range of $0 - $10,000 to all incomes
below $80,000.  Since all cases of incomes exceeding $80,000 were
lumped together  in  the  survey,  we simulated their values by means
of a t(8) distribution. Drawing values from the upper 38% tail of t(8),
we evaluated the values of income as $60,000 + 25,000*t(8).
Other variables: We added  normal distributed variates, with 0 mean and
variance  0.5 to the variables.

We assume that the agency releases information about all
variables, except for Attitudes (towards Jews), which  is unavailable to
the intruder and is at the center of the intruder's investigation.

We denote the released data by

Z = (( z(i,j) ))   with i=1,..,662; j=1,2,3,4.

We assume that the intruder's data, X, are accurate and are related to Z via
the following transformation:
x(0,j) = z(i,j)*theta(i,j) + xi(j),

where theta(i,j) is a bias removing parameter normally distributed with mean 1
and variance  v(j), and xi(j) is normally distributed disturbance with 0  mean
and variance sigma2(j).

The following table provides the values of
v(j), sigma2(j) used in the study:

v(j)     sigma2(j)
Civil                      0.1732       25
Can/US                     0.1732       25
Age                        0.1732       9
Income (in $10000's)       0.1732       4


We first generated several realizations of the above transformation on small
subsets of the data to ascertain the impact of the process of the error
on the data. In Table 4-1 in the paper we present 10 records the the intruder's
accurate data, X, and the biased and corrupted released data, Z, which we
obtained from one realization of the transformation.

Section 4.2 of the paper contains details of the implementation of our Bayesian
model.

Data Used in the Computations:
=============================
We conducted a complete simulation of the procedures for the complete set of
662 cases.  We considered four different scenarios for the simulation. (The
names of datasets used in each of the scenarios appear in brackets below. The
datasets are appended to this text.)

* The released data contains no bias or noise (i.e. v(j)=0 and sigma2(j)=0 for
all j). [Z.DATA]
* The released data contains only noise (i.e., v(j)=0 for all j and
and $sigma2(j)$ as given in the above Table). [X_NOISE.DATA]
* The released data contains only bias (i.e., sigma2(j)=0 for all j and v(j)
as given in the above Table). [X_BIAS.DATA]
* The released data contains both bias and noise (i.e., v(j) and sigma2(j) as
given in the above Table). [X_TAMPERED.DATA]

We took each individual in turn as the object of the intruder's efforts and
carried out the calculations.

Structure of the Datasets:
- -------------------------
Each attached dataset consists of four space-separated columns containing the
data on Age, Civil, Can/US and Income ($) respectively.



Dataset: Z


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/699) of an [OpenML dataset](https://www.openml.org/d/699). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/699/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/699/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/699/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

